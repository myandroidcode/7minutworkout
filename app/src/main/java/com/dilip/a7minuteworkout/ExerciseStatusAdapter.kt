package com.dilip.a7minuteworkout

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_erercise_status.view.*

/**
 * Created by Dilip on 23/03/21.
 */
class ExerciseStatusAdapter(val exerciseList: ArrayList<ExerciseModel>, val context: Context) :
    RecyclerView.Adapter<ExerciseStatusAdapter.ExerciseStatusViewHolder>() {
    class ExerciseStatusViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvItem = view.tv_exercise_status
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseStatusViewHolder {
        return ExerciseStatusViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_erercise_status, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ExerciseStatusViewHolder, position: Int) {
        val exerciseModel: ExerciseModel = exerciseList[position]
        holder.tvItem.text = exerciseModel.getId().toString()

        if (exerciseModel.getIsSelected()) {
            holder.tvItem.background =
                ContextCompat.getDrawable(context, R.drawable.shape_tv_exercise_thin_color)
            holder.tvItem.setTextColor(Color.parseColor("#212121"))
        } else if (exerciseModel.getIsCompleted()) {
            holder.tvItem.background =
                ContextCompat.getDrawable(context, R.drawable.shape_tv_exercise_accent_color)
            holder.tvItem.setTextColor(Color.parseColor("#FFFFFF"))
        } else {
            holder.tvItem.background =
                ContextCompat.getDrawable(context, R.drawable.shape_tv_exercise)
            holder.tvItem.setTextColor(Color.parseColor("#212121"))
        }
    }

    override fun getItemCount() = exerciseList.size


}