package com.dilip.a7minuteworkout

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.ac_toolbar.*
import kotlinx.android.synthetic.main.activity_bmi_calculator.*
import java.math.BigDecimal
import java.math.RoundingMode

class BmiCalculatorActivity : AppCompatActivity() {

    val METRIC_VIEW = "METRIC_UNIT_VIEW"
    val US_UNIT_VIEW = "US_UNIT_VIEW"
    var currentVisibleView = METRIC_VIEW
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmi_calculator)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Calculate BMI"
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        calculate.setOnClickListener {
            if (currentVisibleView == METRIC_VIEW) {
                calculateMetricUnit()
            } else {
                calculateUsUnit()
            }
        }
        changeUnit()

    }

    private fun calculateUsUnit() {
        if (validateUsUnit()) {
            calculateUsBmi()
        } else {
            showToast()
        }
    }

    private fun calculateUsBmi() {
        val feet = et_feet.text.toString()
        val inch = et_inch.text.toString()
        val weight = et_us_weight.text.toString().toFloat()
        val height = inch.toFloat() + feet.toFloat() * 12
        val bmi = 703 * (weight / (height * height))
        displayBMIResult(bmi)
    }

    private fun showToast() {
        Toast.makeText(this, "Please enter valid values.", Toast.LENGTH_SHORT)
            .show()
    }

    private fun calculateMetricUnit() {
        if (validate()) {
            calculateBmi()
        } else {
            showToast()
        }
    }

    private fun calculateBmi() {
        val weight = et_weight.text.toString().toFloat()
        val height = et_height.text.toString().toFloat() / 100
        val bmi = weight / (height * height)
        displayBMIResult(bmi = bmi)

    }

    private fun displayBMIResult(bmi: Float) {

        val bmiLabel: String
        val bmiDescription: String

        if (bmi.compareTo(15f) <= 0) {
            bmiLabel = "Very severely underweight"
            bmiDescription = "Oops! You really need to take better care of yourself! Eat more!"
        } else if (bmi.compareTo(15f) > 0 && bmi.compareTo(16f) <= 0
        ) {
            bmiLabel = "Severely underweight"
            bmiDescription = "Oops!You really need to take better care of yourself! Eat more!"
        } else if (bmi.compareTo(16f) > 0 && bmi.compareTo(18.5f) <= 0
        ) {
            bmiLabel = "Underweight"
            bmiDescription = "Oops! You really need to take better care of yourself! Eat more!"
        } else if (bmi.compareTo(18.5f) > 0 && bmi.compareTo(25f) <= 0
        ) {
            bmiLabel = "Normal"
            bmiDescription = "Congratulations! You are in a good shape!"
        } else if (java.lang.Float.compare(bmi, 25f) > 0 && java.lang.Float.compare(
                bmi,
                30f
            ) <= 0
        ) {
            bmiLabel = "Overweight"
            bmiDescription = "Oops! You really need to take care of your yourself! Workout maybe!"
        } else if (bmi.compareTo(30f) > 0 && bmi.compareTo(35f) <= 0
        ) {
            bmiLabel = "Obese Class | (Moderately obese)"
            bmiDescription = "Oops! You really need to take care of your yourself! Workout maybe!"
        } else if (bmi.compareTo(35f) > 0 && bmi.compareTo(40f) <= 0
        ) {
            bmiLabel = "Obese Class || (Severely obese)"
            bmiDescription = "OMG! You are in a very dangerous condition! Act now!"
        } else {
            bmiLabel = "Obese Class ||| (Very Severely obese)"
            bmiDescription = "OMG! You are in a very dangerous condition! Act now!"
        }

//        tvYourBmi.visibility = View.VISIBLE
//        tvBMIValue.visibility = View.VISIBLE
//        tvBMIType.visibility = View.VISIBLE
//        tvBMIDescription.visibility = View.VISIBLE
        llDisplayBmiResult.visibility = View.VISIBLE
        // This is used to round the result value to 2 decimal values after "."
        val bmiValue = BigDecimal(bmi.toDouble()).setScale(2, RoundingMode.HALF_EVEN).toString()
        tvBMIValue.text = bmiValue // Value is set to TextView
        tvBMIType.text = bmiLabel // Label is set to TextView
        tvBMIDescription.text = bmiDescription // Description is set to TextView
    }

    private fun validate(): Boolean {
        var isValid = true
        if (et_weight.text.toString().isEmpty()) {
            isValid = false
        } else if (et_height.text.toString().isEmpty()) {
            isValid = false
        }
        return isValid
    }

    private fun changeUnit() {
        rgUnits.setOnCheckedChangeListener { rg, p1 ->
            when (rg?.checkedRadioButtonId) {
                R.id.rb_metric_units -> {
                    makeMetricUnitsVisible()
                }
                R.id.rb_us_units -> {
                    makeUsUnitsVisible()
                }
            }
        }
    }

    private fun makeMetricUnitsVisible() {
        currentVisibleView = METRIC_VIEW
        ll_weith_kg.visibility = View.VISIBLE
        ll_height.visibility = View.VISIBLE

        et_height.text?.clear()
        et_weight.text?.clear()

        llDisplayBmiResult.visibility = View.INVISIBLE
        ll_us_unit.visibility = View.GONE
        til_us_weight.visibility = View.GONE
    }

    private fun makeUsUnitsVisible() {
        currentVisibleView = US_UNIT_VIEW
        til_us_weight.visibility = View.VISIBLE
        ll_us_unit.visibility = View.VISIBLE

        et_us_weight.text?.clear()
        et_feet.text?.clear()
        et_inch.text?.clear()

        ll_height.visibility = View.GONE
        ll_weith_kg.visibility = View.GONE
        llDisplayBmiResult.visibility = View.INVISIBLE

    }

    private fun validateUsUnit(): Boolean {
        var isValid = true
        when {
            et_us_weight.text.toString().isEmpty() -> isValid = false
            et_feet.text.toString().isEmpty() -> isValid = false
            et_inch.text.toString().isEmpty() -> isValid = false

        }
        return isValid
    }
}
