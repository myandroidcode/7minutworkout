package com.dilip.a7minuteworkout

import android.app.Dialog
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.ac_toolbar.*
import kotlinx.android.synthetic.main.activity_exercise.*
import kotlinx.android.synthetic.main.dialog_confirm.*
import java.util.*

class ExerciseActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    private var restTimer: CountDownTimer? = null
    private var restProgress = 0
    private var exerciseTimer: CountDownTimer? = null
    private var exerciseProgress = 0

    private var exerciseList: ArrayList<ExerciseModel>? = null
    private var currentExercisePosition = -1
    private var exerciseTimeDuration: Long = 30
    private var restTimerDuration: Long = 10

    private var LOG_TEXT = "log"

    private var tts: TextToSpeech? = null
    private var player: MediaPlayer? = null
    private lateinit var exerciseStatusAdapter: ExerciseStatusAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
//            onBackPressed()
            exitExerciseConfirmation()
        }
        tts = TextToSpeech(this, this)
        exerciseList = ExerciseConstants.defaultExerciseList()
        setupRestView()
        setRecyclerViewAdapter()
    }

    private fun exitExerciseConfirmation() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_confirm)
        dialog.btn_no.setOnClickListener {
            dialog.dismiss()
        }

        dialog.btn_yes.setOnClickListener {
            finish()
        }
        dialog.show()
    }

    private fun setRecyclerViewAdapter() {
        exerciseStatusAdapter = ExerciseStatusAdapter(exerciseList!!, context = this)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvExerciseStatus.layoutManager = linearLayoutManager
        rvExerciseStatus.adapter = exerciseStatusAdapter
    }

    public override fun onDestroy() {
        if (restTimer != null) {
            restTimer!!.cancel()
            restProgress = 0
        }

        if (exerciseTimer != null) {
            exerciseTimer!!.cancel()
            exerciseProgress = 0
        }
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }
        if (player != null) {
            player!!.stop()
        }
        super.onDestroy()
    }


    private fun setupRestView() {
        try {
            player = MediaPlayer.create(this, R.raw.press_start)
            player!!.isLooping = false
            player!!.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // Here according to the view make it visible as this is Rest View so rest view is visible and exercise view is not.
        llRestView.visibility = View.VISIBLE
        llExerciseView.visibility = View.GONE

        if (restTimer != null) {
            restTimer!!.cancel()
            restProgress = 0
        }
        tvUpcomingExerciseName.text = exerciseList!![currentExercisePosition + 1].getName()

        // This function is used to set the progress details.
        setRestProgressBar()
    }


    private fun setRestProgressBar() {

        progressBar.progress = restProgress // Sets the current progress to the specified value.


        // Here we have started a timer of 10 seconds so the 10000 is milliseconds is 10 seconds and the countdown interval is 1 second so it 1000.
        restTimer = object : CountDownTimer(restTimerDuration * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                restProgress++ // It is increased to ascending order
                progressBar.progress = 10 - restProgress // Indicates progress bar progress
                tvTimer.text =
                    (10 - restProgress).toString()  // Current progress is set to text view in terms of seconds.
            }

            override fun onFinish() {
                currentExercisePosition++
                exerciseList!![currentExercisePosition].setIsSelected(true)
                exerciseStatusAdapter.notifyDataSetChanged()
                setupExerciseView()
            }
        }.start()
    }


    private fun setupExerciseView() {

        // Here according to the view make it visible as this is Exercise View so exercise view is visible and rest view is not.
        llRestView.visibility = View.GONE
        llExerciseView.visibility = View.VISIBLE

        if (exerciseTimer != null) {
            exerciseTimer!!.cancel()
            exerciseProgress = 0
        }
        speakOut(exerciseList!![currentExercisePosition].getName())


        ivImage.setImageResource(exerciseList!![currentExercisePosition].getImage())
        tvExerciseName.text = exerciseList!![currentExercisePosition].getName()
        // END

        setExerciseProgressBar()
    }

    private fun speakOut(message: String) {
        tts!!.speak(message, TextToSpeech.QUEUE_FLUSH, null)
    }


    private fun setExerciseProgressBar() {

        progressBarExercise.progress = exerciseProgress

        exerciseTimer = object : CountDownTimer(exerciseTimeDuration * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                exerciseProgress++
                progressBarExercise.progress = exerciseTimeDuration.toInt() - exerciseProgress
                tvExerciseTimer.text = (exerciseTimeDuration - exerciseProgress).toString()
            }

            override fun onFinish() {
                // START
//                if (currentExercisePosition < exerciseList?.size!! - 1) {

                if (currentExercisePosition < 2) {
                    exerciseList!![currentExercisePosition].setIsSelected(false)
                    exerciseList!![currentExercisePosition].setIsCompleted(true)
                    exerciseStatusAdapter.notifyDataSetChanged()
                    setupRestView()
                } else {

                    finish()
                    val intent = Intent(this@ExerciseActivity, ExerciseFinishActivity::class.java)
                    startActivity(intent)

//                    Toast.makeText(
//                        this@ExerciseActivity,
//                        "Congratulations! You have completed the 7 minutes workout.",
//                        Toast.LENGTH_SHORT
//                    ).show()
                }
                // END
            }
        }.start()
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_NOT_SUPPORTED || result == TextToSpeech.LANG_MISSING_DATA) {
                Log.d(LOG_TEXT, "The Language specified not supported!")
            }
        } else {
            Log.d(LOG_TEXT, "Initialization failed!")

        }

    }

}